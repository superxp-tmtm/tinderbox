// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

//player createDiaryRecord ["Diary", ["Assets", "Example Mission Assets"]];
//player createDiaryRecord ["Diary", ["Mission", "Example Mission Briefing"]];
//player createDiaryRecord ["Diary", ["Situation", "Example Situation Briefing"]];

player createDiaryRecord ["Diary", ["Assets", "Available assets:
	<br/>- 1x USS Liberty
	<br/>- 1x MH-60S Knighthawk DAP
	<br/>- 8x Prowler (Unarmed)
	<br/>- 2x Speedboat Minigun
	<br/>- 1x Towing Tractor (Rearm/Repair/Refuel vehicle)
"]];

player createDiaryRecord ["Diary", ["Intel",
"You’ll have one of our Knighthawks on station for additional fire support and logistics, make that Ace earn their seat. Be warned however, the enemy has an AA net set up in their base on this side of the island.
<br/><br/>In addition, we’ll have the support of the USS Liberty in taking down any hard targets that stand in our way. In order to utilize this support, NATO was kind enough to give us a few laser designators and a terminal that’s hooked into the Liberty’s weapon systems. Just call it in to your ground commander and he’ll hook you up with some of the finest ordinance America is willing to let us use."
]];

player createDiaryRecord ["Diary", ["Mission",
"The first thing you should prioritize doing when you land is knocking out two key radio facilities, that should help cripple the enemy’s communications network.
If NATO does their part and hits the rest of the island at the same time, CSAT won’t even get word out that we’re here. After that, hit the facilities in any order you want.
<br/><br/>Good luck and godspeed out there men. If we do good on this job, there’s surely more money for us in the near future. NATO seems desperate to ramp this war up, which is damn good for our profit margins."
]];

player createDiaryRecord ["Diary", ["Situation",
"Listen up people, we’ve got a priority mission from our contacts at NATO and they’re shelling out some big money for this operation. They want us to aid in their invasion of CSAT occupied Malden.
<br/><br/>Now I know some of you are thinking that we’re just going to be there to take the brunt of the landing, but I can assure you that isn’t the case. We’re going to be an auxiliary landing force, focused on taking some key locations on the southeastern side of the island."
]];