// Function for updating mission tasks when objectives are completed.
if (!isServer) exitWith {};
switch (toLower (_this select 0)) do {
	case "radio_south_destroyed": {
		["tsk_radio_south", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		if (("tsk_radio_south" call BIS_fnc_taskCompleted) AND ("tsk_radio_east" call BIS_fnc_taskCompleted)) then {
			["tsk_radio_towers", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		};
		["mission_end"] call SXP_fnc_updateTask;
	};
	case "radio_east_destroyed": {
		["tsk_radio_east", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		if (("tsk_radio_south" call BIS_fnc_taskCompleted) AND ("tsk_radio_east" call BIS_fnc_taskCompleted)) then {
			["tsk_radio_towers", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		};
		["mission_end"] call SXP_fnc_updateTask;
	};
	case "military_base": {
		["tsk_military_base", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		["mission_end"] call SXP_fnc_updateTask;
	};
	case "civilian_harbour": {
		["tsk_civilian_harbour", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		["mission_end"] call SXP_fnc_updateTask;
	};
	case "industrial_area": {
		["tsk_industrial_area", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		["mission_end"] call SXP_fnc_updateTask;
	};
	case "vip_located": {
		if (!(["tsk_capture_vip"] call BIS_fnc_taskExists)) then {
			[true, ["tsk_capture_vip", ""], "capture_vip", getMarkerPos "marker_capture_vip", "CREATED",50,true,"meet",true] call BIS_fnc_taskCreate;
		};
	};
	case "vip_killed": {
		// If the task does not exist, create it with a failed state
		if (!(["tsk_capture_vip"] call BIS_fnc_taskExists)) then {
			[true, ["tsk_capture_vip", ""], "capture_vip", getMarkerPos "marker_capture_vip", "FAILED",50,true,"meet",true] call BIS_fnc_taskCreate;
		};
		// We don't care about the VIP once we have the intel
		if (!("tsk_capture_vip" call BIS_fnc_taskCompleted)) then {
			["tsk_capture_vip", "FAILED", true] call BIS_fnc_taskSetState;
		};
		["mission_end"] call SXP_fnc_updateTask;
	};
	case "vip_questioned": {
		// If the task does not exist, create it with a succeeded state
		if (!(["tsk_capture_vip"] call BIS_fnc_taskExists)) then {
			[true, ["tsk_capture_vip", ""], "capture_vip", getMarkerPos "marker_capture_vip", "SUCCEEDED",50,true,"meet",true] call BIS_fnc_taskCreate;
		};
		if (!("tsk_capture_vip" call BIS_fnc_taskCompleted)) then {
			["tsk_capture_vip", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		};
		["mission_end"] call SXP_fnc_updateTask;
	};
	case "mission_end": {
		// Only end the mission once
		if (isNil "sxp_mission_ended") then {
			// Check if our prerequisite tasks are done
			if (("tsk_radio_towers" call BIS_fnc_taskCompleted) AND ("tsk_military_base" call BIS_fnc_taskCompleted) AND ("tsk_civilian_harbour" call BIS_fnc_taskCompleted) AND ("tsk_industrial_area" call BIS_fnc_taskCompleted)) then {
				// Check to see which state the VIP task is in. End the mission if the task is done
				if (("tsk_capture_vip" call BIS_fnc_taskState) == "SUCCEEDED") then {
					// Good ending
					sxp_mission_ended = true;
					["good_end",true,true,true] remoteExec ["BIS_fnc_endMission", 0, true];
				};
				if (("tsk_capture_vip" call BIS_fnc_taskState) == "FAILED") then {
					// Bad ending
					sxp_mission_ended = true;
					["bad_end",false,true,true] remoteExec ["BIS_fnc_endMission", 0, true];
				};
			};
		};
	};
};