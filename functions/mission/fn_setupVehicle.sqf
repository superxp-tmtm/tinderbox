// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	// MH-60 DAP
	case (toLower "CUP_B_MH60L_DAP_2x_USN"): {
		// Configure the appearance
		[
			_newVeh,
			["Black",1], 
			["Navyclan_hide",1,"Navyclan2_hide",1,"Filters_Hide",1,"mainRotor_folded",1,"mainRotor_unfolded",0,"Hide_ESSS2x",0,"Hide_ESSS4x",1,"Hide_FlirTurret",0,"Hide_Nose",1,"Blackhawk_Hide",0,"Hide_Probe",0,"Doorcock_Hide",0]
		] call BIS_fnc_initVehicle;
		
		// Configure pylons
		_newVeh setPylonLoadout ["pylons2", "CUP_PylonPod_1200Rnd_TE1_Red_Tracer_30x113mm_M789_HEDP_M", false, []];
		_newVeh setPylonLoadout ["pylons3", "CUP_PylonPod_19Rnd_CRV7_FAT_M", false, []];
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;
		
		// Set vehicle cargo
		[_newVeh, "ammo_box"] call XPT_fnc_loadItemCargo;
	};
	
	// Prowler
	case (toLower "B_LSV_01_unarmed_F"): {
		// Configure the appearance
		[
			_newVeh,
			["Black",1], 
			["HideDoor1",0,"HideDoor2",0,"HideDoor3",0,"HideDoor4",0]
		] call BIS_fnc_initVehicle;
		[_newVeh, "ammo_box"] call XPT_fnc_loadItemCargo;
	};
	
	// Speedboat Minigun
	case (toLower "B_T_Boat_Armed_01_minigun_F"): {
		
	};
	
	// Mk41 VLS
	case (toLower "B_Ship_MRLS_01_F"): {
		// Disable autonomy
		_newVeh setAutonomous false;
	};
	
	// Mk45 Hammer
	case (toLower "B_Ship_Gun_01_F"): {
		// Disable autonomy
		_newVeh setAutonomous false;
		[_newVeh, {
			_this setVehicleAmmo 0;
			_this addMagazine ["magazine_ShipCannon_120mm_HE_shells_x32", 12];
			_this addMagazines ["magazine_ShipCannon_120mm_smoke_shells_x6", 4];
			reload _this}
		] remoteExec ["BIS_fnc_call", _newVeh]
	};
	
	// NATO Ammo Crate
	case (toLower "Box_NATO_Ammo_F"): {
		// Load inventory
		[_newVeh, "ammo_box"] call XPT_fnc_loadItemCargo;
	};
	
	// Medical Crate
	case (toLower "ACE_medicalSupplyCrate"): {
		// Load inventory
		[_newVeh, "medical_box"] call XPT_fnc_loadItemCargo;
	};
	
	// Explosives Crate
	case (toLower "Box_NATO_AmmoOrd_F"): {
		// Load inventory
		[_newVeh, "explosive_box"] call XPT_fnc_loadItemCargo;
	};
	
	case (toLower "CUP_B_TowingTractor_NATO"): {
		[_newVeh, -10] call ace_refuel_fnc_makeSource;
		[_newVeh, 1500] call ace_rearm_fnc_makeSource;
		_newVeh setVariable ['ace_isRepairVehicle', 1, true];
	};
};