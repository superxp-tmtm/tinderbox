// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point

// Uncomment the line below to use briefing.sqf for mission briefings. Un-needed if you're using XPTBriefings.hpp
[] execVM "scripts\briefing.sqf";

// Add the holdaction to the VIP
if (hasInterface) then {
	if (!(isNil "unit_vip")) then {
		[
			unit_vip,
			"interrogate VIP",
			"media\holdActions\holdAction_alert.paa",
			"media\holdActions\holdAction_alert.paa",
			"(!('tsk_capture_vip' call BIS_fnc_taskCompleted)) && ((player distance _target) < 5)",
			"(!('tsk_capture_vip' call BIS_fnc_taskCompleted)) && ((player distance _target) < 5)",
			nil,
			nil,
			{
				params ["_target", "_caller", "_actionId", "_arguments"];
				["vip_questioned"] remoteExec ["SXP_fnc_updateTask", 2];
				[_target, true, _caller] remoteExec ["ace_captives_fnc_sethandcuffed", _target];
			},
			nil,
			[],
			10, // 10 seconds to interrogate the VIP
			1000, // Maximum priority
			false,
			false
		] call BIS_fnc_holdActionAdd;
	};
	
	[
		intel_laptop,
		"search laptop",
		"media\holdActions\holdAction_usb.paa",
		"media\holdActions\holdAction_usb.paa",
		"(!(['tsk_capture_vip'] call BIS_fnc_taskExists)) && ((player distance _target) < 5)",
		"(!(['tsk_capture_vip'] call BIS_fnc_taskExists)) && ((player distance _target) < 5)",
		nil,
		nil,
		{["vip_located"] remoteExec ["SXP_fnc_updateTask", 2];},
		nil,
		[],
		5, // 5 seconds to search the laptop
		1000, // Maximum priority
		false,
		false
	] call BIS_fnc_holdActionAdd;
};