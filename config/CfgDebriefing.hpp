// Mission endings
// Handles the mission ending screen
// https://community.bistudio.com/wiki/Debriefing

class example
{
	title = "Example Ending"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "The mission maker should change this"; // Subtitle below the title when the closing shot is triggered
	description = "This should be changed before the mission is finished"; // Description visible on the debriefing screen after the closing shot
	//pictureBackground = ""; // Image file used as a background on the debriefing screen
	//picture = ""; // Icon used for the closing shot
	//pictureColor[] = {1,1,1,1}; // Colour of the icon during the closing shot. Leave as default most of the time
};

class good_end
{
	title = "Mission Complete";
	subtitle = "Good work people, expect a big bonus";
	description = "Turns out that shithead was a lead scientist on a massive CSAT research project, we’ve got word from NATO high command that they want to keep us on this job. Thanks to your efforts, we now know the exact positions of the research sites he was involved with. We’ve got a few days to prepare so rest up and get ready for the next one.";
};

class bad_end
{
	title = "Mission Complete";
	subtitle = "Goddamnit people, NATO needed that intel!";
	description = "We fucked up, big time. NATO wanted whatever information that shithead had in his noggin because of some top secret CSAT project they want their grubby ass hands on. Now they’re going to send us in, blind as fucking bats, to try and clean up this mess.";
};