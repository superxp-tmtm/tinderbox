// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"CUP_arifle_HK_M27","","cup_acc_flashlight","optic_holosight_blk_f",{"CUP_30Rnd_556x45_Emag_Tracer_Red",30},{},""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"CUP_hgun_M17_Black","","cup_acc_cz_m3x","",{"CUP_21Rnd_9x19_M17_Black",21},{},""};
		binocular = "Binocular";
		
		uniformClass = "tmtm_u_combatUniform_nwupat";
		headgearClass = "tmtm_h_helmetEnhanced_grey";
		facewearClass = "CUP_G_ESS_BLK_Scarf_Blk";
		vestClass = "V_PlateCarrier1_blk";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemMicroDAGR", "TFAR_anprc152", "ItemCompass", "ItemWatch", ""};
		
		uniformItems[] = {{"ItemcTabHCam",1},{"ACE_EntrenchingTool",1},{"CUP_21Rnd_9x19_M17_Black",2,21}};
		vestItems[] = {{"HandGrenade",2,1},{"SmokeShell",2,1},{"SmokeShellGreen",1,1},{"SmokeShellRed",1,1},{"CUP_30Rnd_556x45_Emag_Tracer_Red",8,30}};
		backpackItems[] = {};
		
		basicMedUniform[] = {{"ACE_fieldDressing",15},{"ACE_morphine",1},{"ACE_epinephrine",2},{"ACE_splint",2}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class B_Officer_F: base
	{
		displayName = "B_officer_F";

		binocular = "Laserdesignator_01_khk_F";

		headgearClass = "H_MilCap_blue";
		facewearClass = "G_Aviator";
		backpackClass = "FRXA_tf_rt1523g_Black";

		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch",""};

		backpackItems[] = {{"ACE_CableTie",5},{"ACE_wirecutter",1}};

		basicMedBackpack[] = {{"ACE_bloodIV_500",2}};
	};
	
	class B_medic_F: base
	{
		displayName = "B_medic_F";

		backpackClass = "B_AssaultPack_blk";
		
		backpackItems[] = {};

		basicMedBackpack[] = {{"ACE_bloodIV_500",8},{"ACE_fieldDressing",70},{"ACE_epinephrine",25},{"ACE_morphine",15},{"ACE_personalAidKit",1}};

	};
	
	class cmd_medic: B_medic_F
	{
		displayName = "B_medic_F";

		backpackClass = "FRXA_tf_rt1523g_big_Black";

		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
	
	class B_soldier_UAV_F: base
	{
		displayName = "B_soldier_UAV_F";

		backpackClass = "FRXA_tf_rt1523g_Black";

		linkedItems[] = {"ItemMap","B_UavTerminal","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
	
	class B_Helipilot_F: base
	{
		displayName = "B_Helipilot_F";

		primaryWeapon[] = {"CUP_smg_MP7","","cup_acc_flashlight","CUP_optic_MRad",{"CUP_40Rnd_46x30_MP7_Red_Tracer",40},{},""};

		headgearClass = "H_CrewHelmetHeli_B";
		facewearClass = "";
		vestClass = "tmtm_v_tacticalvest_mercblue";
		backpackClass = "FRXA_tf_rt1523g_big_Black";

		vestItems[] = {{"HandGrenade",2,1},{"SmokeShellGreen",4,1},{"SmokeShellRed",1,1},{"CUP_40Rnd_46x30_MP7_Red_Tracer",2,40},{"SmokeShell",5,1}};
		backpackItems[] = {{"ToolKit",1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class pilot_lead: B_Helipilot_F
	{
		displayName = "B_Helipilot_F";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
	
	class B_Soldier_SL_F: base
	{
		displayName = "B_Soldier_SL_F";
		
		backpackClass = "FRXA_tf_rt1523g_Black";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_anprc152","ItemCompass","ItemWatch",""};

		backpackItems[] = {{"ACE_CableTie",5},{"ACE_wirecutter",1}};

		basicMedBackpack[] = {{"ACE_bloodIV_500",2}};
	};
	
	class B_Soldier_TL_F: base
	{
		displayName = "B_Soldier_TL_F";

		primaryWeapon[] = {"CUP_arifle_HK_M27_AG36","","cup_acc_flashlight","optic_holosight_blk_f",{"CUP_30Rnd_556x45_Emag_Tracer_Red",30},{"1Rnd_HE_Grenade_shell",1},""};
		
		backpackClass = "B_AssaultPack_blk";

		backpackItems[] = {{"ACE_HuntIR_monitor",1},{"ACE_CableTie",5},{"ACE_wirecutter",1},{"ACE_HuntIR_M203",6,1},{"1Rnd_Smoke_Grenade_shell",6,1},{"1Rnd_SmokeRed_Grenade_shell",3,1},{"1Rnd_SmokeGreen_Grenade_shell",3,1},{"1Rnd_HE_Grenade_shell",5,1}};

		basicMedBackpack[] = {{"ACE_bloodIV_500",2}};

	};
	
	class B_recon_JTAC_F: base
	{
		displayName = "B_recon_JTAC_F";

		binocular = "Laserdesignator_01_khk_F";

		backpackClass = "FRXA_tf_rt1523g_Black";
	};
	
	class B_Soldier_M_F: base
	{
		displayName = "B_soldier_M_F";

		primaryWeapon[] = {"CUP_arifle_HK_M27","","cup_acc_flashlight","cup_optic_elcan_specterdr_black_3d",{"CUP_30Rnd_556x45_Emag_Tracer_Red",30},{},"bipod_01_f_blk"};
	};
	
	class B_Soldier_AR_F: base
	{
		displayName = "B_soldier_AR_F";

		primaryWeapon[] = {"CUP_lmg_Mk48_nohg","","cup_acc_flashlight","optic_holosight_blk_f",{"CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M",100},{},""};

		backpackClass = "B_AssaultPack_blk";

		vestItems[] = {{"HandGrenade",2,1},{"SmokeShell",2,1},{"SmokeShellGreen",1,1},{"SmokeShellRed",1,1},{"CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M",2,100}};
		backpackItems[] = {{"CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M",2,100}};
	};
	
	class B_Soldier_GL_F: base
	{
		displayName = "B_Soldier_GL_F";

		primaryWeapon[] = {"CUP_arifle_HK_M27_AG36","","cup_acc_flashlight","optic_holosight_blk_f",{"CUP_30Rnd_556x45_Emag_Tracer_Red",30},{"1Rnd_HE_Grenade_shell",1},""};

		backpackClass = "B_AssaultPack_blk";

		backpackItems[] = {{"1Rnd_Smoke_Grenade_shell",3,1},{"1Rnd_HE_Grenade_shell",24,1}};
	};
	
	class B_Soldier_LAT_F: base
	{
		displayName = "B_soldier_LAT_F";

		//secondaryWeapon[] = {"launch_MRAWS_green_F","","","",{"MRAWS_HEAT_F",1},{},""};
		secondaryWeapon[] = {"CUP_launch_HCPF3","","","",{},{},""};

		backpackClass = "B_AssaultPack_blk";

		//backpackItems[] = {{"MRAWS_HEAT_F",2,1},{"MRAWS_HE_F",1,1}};
		backpackItems[] = {{{"CUP_launch_HCPF3","","","",{},{},""},1}};
	};
};