// Task Descriptions
// Can be used for moving task descriptions from script files to configs


// Create new classes under here. BIS_fnc_taskCreate will use either the class passed as the description parameter (in string format)
// The following would be loaded by setting the description to "example";
// If the description is blank (""), then the task ID will be used automatically
class example
{
	title = "Example Task Title";					// Title of task. Displayed as the task name
	description = "Example Task Description";		// Description of task. Additional details displayed when the task is selected on the map screen.
	marker = "";									// Task marker. Leave blank
};

class radio_towers
{
	title = "Disable CSAT radios";
	description = "Cut off CSAT’s communications both on and off the island, that way they don’t make our day worse with any reinforcements.";
};

class radio_tower_east
{
	title = "Destroy the eastern radio tower";
	description = "The eastern radio tower on the military island needs to be destroyed.";
};

class radio_tower_south
{
	title = "Destroy the southern radio tower";
	description = "The southern radio tower on the military island needs to be destroyed.";
};

class military_base
{
	title = "Secure Military Base";
	description = "Clear out CSAT’s main presence on this side of the island and cut off their supply lines. NATO will pay us more the more intact this place is.";
};

class civilian_harbour
{
	title = "Secure Main Harbor";
	description = "Cut off CSAT’s access to the oceans so Strike Group 14 has free reign of the waters. Don’t damage it too badly or they’ll knock our pay down a few notches.";
};

class industrial_area
{
	title = "Secure Industrial Area";
	description = "Clear out and CSAT forces in the civilian industrial area. It should be evacuated at this point, but we want to make sure that the people have something to come back to. Hearts and minds and all that crap.";
};

class capture_vip
{
	title = "Capture the VIP";
	description = "Intel has pointed us to one of CSAT’s lead researchers being in our sector, NATO will pay us good money if we can get any information out of him.";
};