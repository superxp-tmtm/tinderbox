// onPlayerRespawn.sqf
// Executes on a player's machine when they respawn
// _this = [<newUnit>, <oldUnit>, <respawn>, <respawnDelay>]
_this params ["_newUnit", "_oldUnit", "_respawn", "_respawnDelay"];

// Call the template onPlayerRespawn function
_this call XPT_fnc_onPlayerRespawn; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point

// Enable datalink sending for players
_newUnit setVehicleReportRemoteTargets true;

if (!isNull _oldUnit) then {
	if (((getPosASL _newUnit) select 2) < 2) then {
		private _pos = getPosASL _newUnit;
		_pos set [2,9];
		_newUnit setPosASL _pos;
	};
};